package com.huohuzhihui.web.controller.merchant;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.merchant.domain.MerCostReport;
import com.huohuzhihui.merchant.domain.MerGoods;
import com.huohuzhihui.merchant.domain.MerGoodsOrder;
import com.huohuzhihui.merchant.service.IMerCostService;
import com.huohuzhihui.merchant.service.IMerGoodsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户商品订单Controller
 * 
 * @author zylu
 * @date 2020-11-14
 */
@RestController
@RequestMapping("/merchant/order")
public class MerGoodsOrderController extends BaseController
{
    @Autowired
    private IMerGoodsOrderService merGoodsOrderService;
    @Autowired
    private IMerCostService merCostService;


    /**
     * 查询商户商品订单列表
     */
    @PreAuthorize("@ss.hasPermi('merchant:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(MerGoodsOrder merGoodsOrder)
    {
        startPage();
        List<MerGoodsOrder> list = merGoodsOrderService.selectMerGoodsOrderList(merGoodsOrder);
        return getDataTable(list);
    }

    /**
     * 导出商户商品订单列表
     */
    @PreAuthorize("@ss.hasPermi('merchant:order:export')")
    @Log(title = "商户商品订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(MerGoodsOrder merGoodsOrder)
    {
        List<MerGoodsOrder> list = merGoodsOrderService.selectMerGoodsOrderList(merGoodsOrder);
        ExcelUtil<MerGoodsOrder> util = new ExcelUtil<MerGoodsOrder>(MerGoodsOrder.class);
        return util.exportExcel(list, "营收统计");
    }

    /**
     * 获取商户商品订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('merchant:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(merGoodsOrderService.selectMerGoodsOrderById(id));
    }

    /**
     * 新增商户商品订单
     */
    @PreAuthorize("@ss.hasPermi('merchant:order:add')")
    @Log(title = "商户商品订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MerGoodsOrder merGoodsOrder)
    {
        return toAjax(merGoodsOrderService.insertMerGoodsOrder(merGoodsOrder));
    }

    /**
     * 修改商户商品订单
     */
    @PreAuthorize("@ss.hasPermi('merchant:order:edit')")
    @Log(title = "商户商品订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MerGoodsOrder merGoodsOrder)
    {
        return toAjax(merGoodsOrderService.updateMerGoodsOrder(merGoodsOrder));
    }

    /**
     * 删除商户商品订单
     */
    @PreAuthorize("@ss.hasPermi('merchant:order:remove')")
    @Log(title = "商户商品订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(merGoodsOrderService.deleteMerGoodsOrderByIds(ids));
    }

    /**
     * 查询商户商品订单列表
     */
    @PreAuthorize("@ss.hasPermi('merchant:order:report')")
    @GetMapping("/report")
    public TableDataInfo report(Long merchantId, String beginDate, String endDate)
    {
        startPage();
        List<MerCostReport> list = merCostService.getOrderStatisticsByMerchantId( merchantId,  beginDate,  endDate);
        return getDataTable(list);
    }
}
