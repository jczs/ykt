import request from '@/utils/request'

// 查询节次列表
export function listSection(query) {
  return request({
    url: '/base/section/list',
    method: 'get',
    params: query
  })
}

// 查询节次列表
export function selectSection(query) {
  return request({
    url: '/base/section/select',
    method: 'get',
    params: query
  })
}

// 查询节次详细
export function getSection(id) {
  return request({
    url: '/base/section/' + id,
    method: 'get'
  })
}

// 新增节次
export function addSection(data) {
  return request({
    url: '/base/section',
    method: 'post',
    data: data
  })
}

// 修改节次
export function updateSection(data) {
  return request({
    url: '/base/section',
    method: 'put',
    data: data
  })
}

// 删除节次
export function delSection(id) {
  return request({
    url: '/base/section/' + id,
    method: 'delete'
  })
}

// 导出节次
export function exportSection(query) {
  return request({
    url: '/base/section/export',
    method: 'get',
    params: query
  })
}