import request from '@/utils/request'

// 查询专业列表
export function listSpecialty(query) {
  return request({
    url: '/base/specialty/list',
    method: 'get',
    params: query
  })
}

//专业下拉
export function selectSpecialty(query) {
  return request({
    url: '/base/specialty/select',
    method: 'get',
    params: query
  })
}


// 查询专业详细
export function getSpecialty(id) {
  return request({
    url: '/base/specialty/' + id,
    method: 'get'
  })
}

// 新增专业
export function addSpecialty(data) {
  return request({
    url: '/base/specialty',
    method: 'post',
    data: data
  })
}

// 修改专业
export function updateSpecialty(data) {
  return request({
    url: '/base/specialty',
    method: 'put',
    data: data
  })
}

// 删除专业
export function delSpecialty(id) {
  return request({
    url: '/base/specialty/' + id,
    method: 'delete'
  })
}

// 导出专业
export function exportSpecialty(query) {
  return request({
    url: '/base/specialty/export',
    method: 'get',
    params: query
  })
}