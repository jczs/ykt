import request from '@/utils/request'

// 查询门禁设备列表
export function listDevice(query) {
  return request({
    url: '/door/device/list',
    method: 'get',
    params: query
  })
}

// 查询门禁设备详细
export function getDevice(id) {
  return request({
    url: '/door/device/' + id,
    method: 'get'
  })
}

// 新增门禁设备
export function addDevice(data) {
  return request({
    url: '/door/device',
    method: 'post',
    data: data
  })
}

// 修改门禁设备
export function updateDevice(data) {
  return request({
    url: '/door/device',
    method: 'put',
    data: data
  })
}

// 删除门禁设备
export function delDevice(id) {
  return request({
    url: '/door/device/' + id,
    method: 'delete'
  })
}

// 导出门禁设备
export function exportDevice(query) {
  return request({
    url: '/door/device/export',
    method: 'get',
    params: query
  })
}