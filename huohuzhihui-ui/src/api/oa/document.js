import request from '@/utils/request'

// 查询公文列表
export function listDocument(query) {
  return request({
    url: '/oa/document/list',
    method: 'get',
    params: query
  })
}

// 查询个人公文列表
export function myDocument(query) {
  return request({
    url: '/oa/document/my',
    method: 'get',
    params: query
  })
}

// 查询个人待办公文列表
export function todoDocument(query) {
  return request({
    url: '/oa/document/todo',
    method: 'get',
    params: query
  })
}

// 查询公文详细
export function getDocument(id) {
  return request({
    url: '/oa/document/' + id,
    method: 'get'
  })
}

// 新增公文
export function addDocument(data) {
  return request({
    url: '/oa/document',
    method: 'post',
    data: data
  })
}

// 修改公文
export function updateDocument(data) {
  return request({
    url: '/oa/document',
    method: 'put',
    data: data
  })
}

// 删除公文
export function delDocument(id) {
  return request({
    url: '/oa/document/' + id,
    method: 'delete'
  })
}


// 撤销公文
export function rollbackDocument(id) {
  return request({
    url: '/oa/document/rollback' + id,
    method: 'get'
  })
}

// 发送公文
export function sendDocument(id) {
  return request({
    url: '/oa/document/send/' + id,
    method: 'get'
  })
}

// 导出公文
export function exportDocument(query) {
  return request({
    url: '/oa/document/export',
    method: 'get',
    params: query
  })
}
