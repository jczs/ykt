import request from '@/utils/request'

// 查询文档轮阅列表
export function listArchive(query) {
  return request({
    url: '/oa/archive/list',
    method: 'get',
    params: query
  })
}

// 查询文档轮阅详细
export function getArchive(id) {
  return request({
    url: '/oa/archive/' + id,
    method: 'get'
  })
}

// 新增文档轮阅
export function addArchive(data) {
  return request({
    url: '/oa/archive',
    method: 'post',
    data: data
  })
}

// 修改文档轮阅
export function updateArchive(data) {
  return request({
    url: '/oa/archive',
    method: 'put',
    data: data
  })
}

// 删除文档轮阅
export function delArchive(id) {
  return request({
    url: '/oa/archive/' + id,
    method: 'delete'
  })
}

// 导出文档轮阅
export function exportArchive(query) {
  return request({
    url: '/oa/archive/export',
    method: 'get',
    params: query
  })
}