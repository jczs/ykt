import request from '@/utils/request'

// 查询单字段列表
export function listField(query) {
  return request({
    url: '/oa/field/list',
    method: 'get',
    params: query
  })
}


// 查询单字段列表
export function selectField(query) {
  return request({
    url: '/oa/field/select',
    method: 'get',
    params: query
  })
}

// 查询单字段详细
export function getField(id) {
  return request({
    url: '/oa/field/' + id,
    method: 'get'
  })
}

// 新增单字段
export function addField(data) {
  return request({
    url: '/oa/field',
    method: 'post',
    data: data
  })
}

// 修改单字段
export function updateField(data) {
  return request({
    url: '/oa/field',
    method: 'put',
    data: data
  })
}

// 删除单字段
export function delField(id) {
  return request({
    url: '/oa/field/' + id,
    method: 'delete'
  })
}

// 导出单字段
export function exportField(query) {
  return request({
    url: '/oa/field/export',
    method: 'get',
    params: query
  })
}