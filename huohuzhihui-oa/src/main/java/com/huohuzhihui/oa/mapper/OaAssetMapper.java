package com.huohuzhihui.oa.mapper;

import com.huohuzhihui.oa.domain.OaAsset;

import java.util.List;

/**
 * 资产信息Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface OaAssetMapper 
{
    /**
     * 查询资产信息
     * 
     * @param id 资产信息ID
     * @return 资产信息
     */
    public OaAsset selectOaAssetById(Long id);
    
    /**
     * 查询资产信息
     * 
     * @param oaAsset 资产信息
     * @return 资产信息
     */
    public OaAsset findOaAsset(OaAsset oaAsset);
    
    /**
     * 查询资产信息列表
     * 
     * @param oaAsset 资产信息
     * @return 资产信息集合
     */
    public List<OaAsset> selectOaAssetList(OaAsset oaAsset);

    /**
     * 新增资产信息
     * 
     * @param oaAsset 资产信息
     * @return 结果
     */
    public int insertOaAsset(OaAsset oaAsset);

    /**
     * 修改资产信息
     * 
     * @param oaAsset 资产信息
     * @return 结果
     */
    public int updateOaAsset(OaAsset oaAsset);

    /**
     * 删除资产信息
     * 
     * @param id 资产信息ID
     * @return 结果
     */
    public int deleteOaAssetById(Long id);

    /**
     * 批量删除资产信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaAssetByIds(Long[] ids);
}
