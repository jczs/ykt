package com.huohuzhihui.oa.mapper;

import com.huohuzhihui.oa.domain.OaFlowProcess;

import java.util.List;
import java.util.Set;

/**
 * 工作流步骤Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface OaFlowProcessMapper 
{
    /**
     * 查询工作流步骤
     * 
     * @param id 工作流步骤ID
     * @return 工作流步骤
     */
    public OaFlowProcess selectOaFlowProcessById(Long id);

    /**
     * 查询工作流步骤列表
     * 
     * @param oaFlowProcess 工作流步骤
     * @return 工作流步骤集合
     */
    public List<OaFlowProcess> selectOaFlowProcessList(OaFlowProcess oaFlowProcess);

    /**
     * 新增工作流步骤
     * 
     * @param oaFlowProcess 工作流步骤
     * @return 结果
     */
    public int insertOaFlowProcess(OaFlowProcess oaFlowProcess);

    /**
     * 修改工作流步骤
     * 
     * @param oaFlowProcess 工作流步骤
     * @return 结果
     */
    public int updateOaFlowProcess(OaFlowProcess oaFlowProcess);

    /**
     * 删除工作流步骤
     * 
     * @param id 工作流步骤ID
     * @return 结果
     */
    public int deleteOaFlowProcessById(Long id);

    /**
     * 批量删除工作流步骤
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaFlowProcessByIds(Long[] ids);


    /**
     * 查询用户ID列表
     * 
     * @param sql String SQL语句
     * @return 结果
     */
    public Set<Long> selectStepUsers(String sql);
}
