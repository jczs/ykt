package com.huohuzhihui.oa.mapper;

import com.huohuzhihui.oa.domain.OaMeetingUser;

import java.util.List;

/**
 * 与会人员Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface OaMeetingUserMapper 
{
    /**
     * 查询与会人员
     * 
     * @param id 与会人员ID
     * @return 与会人员
     */
    public OaMeetingUser selectOaMeetingUserById(Long id);

    /**
     * 查询与会人员列表
     * 
     * @param oaMeetingUser 与会人员
     * @return 与会人员集合
     */
    public List<OaMeetingUser> selectOaMeetingUserList(OaMeetingUser oaMeetingUser);

    /**
     * 新增与会人员
     * 
     * @param oaMeetingUser 与会人员
     * @return 结果
     */
    public int insertOaMeetingUser(OaMeetingUser oaMeetingUser);

    /**
     * 修改与会人员
     * 
     * @param oaMeetingUser 与会人员
     * @return 结果
     */
    public int updateOaMeetingUser(OaMeetingUser oaMeetingUser);

    /**
     * 删除与会人员
     * 
     * @param meetId 会议ID
     * @return 结果
     */
    public int deleteByMeeting(Long meetId);
    
    /**
     * 删除与会人员
     * 
     * @param id 与会人员ID
     * @return 结果
     */
    public int deleteOaMeetingUserById(Long id);

    /**
     * 批量删除与会人员
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaMeetingUserByIds(Long[] ids);
}
