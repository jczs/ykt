package com.huohuzhihui.oa.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 资产信息对象 oa_asset
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("资产信息实体")
@Getter
@Setter
public class OaAsset extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 编码 */
    @Excel(name = "编码")
    @ApiModelProperty("编码")
    private String code;

    /** 资产名称 */
    @Excel(name = "资产名称")
    @ApiModelProperty("资产名称")
    private String title;

    /** 分类ID */
    @ApiModelProperty("分类ID")
    private Long cateId;
    @Excel(name = "分类")
    private String cateName;

    /** 买入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "买入时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty("买入时间")
    private Date buyTime;

    /** 买入价格 */
    @Excel(name = "买入价格")
    @ApiModelProperty("买入价格")
    private BigDecimal buyPrice;

    /** 当前价格 */
    @Excel(name = "当前价格")
    @ApiModelProperty("当前价格")
    private BigDecimal currentPrice;

    /** 负责人 */
    @Excel(name = "负责人")
    @ApiModelProperty("负责人")
    private String masterUser;

    /** 状态 */
    @Excel(name = "状态", dictType = "asset_status", combo = {"闲置","占用","损坏","丢失","报废"})
    @ApiModelProperty("状态")
    private String status;


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("title", getTitle())
            .append("cateId", getCateId())
            .append("buyTime", getBuyTime())
            .append("buyPrice", getBuyPrice())
            .append("currentPrice", getCurrentPrice())
            .append("masterUser", getMasterUser())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
