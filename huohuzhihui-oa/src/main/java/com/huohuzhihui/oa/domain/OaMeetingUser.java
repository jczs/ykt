package com.huohuzhihui.oa.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 与会人员对象 oa_meeting_user
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("与会人员实体")
@Getter
@Setter
public class OaMeetingUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 会议 */
    @ApiModelProperty("会议")
    private Long meetId;
    @Excel(name = "会议")
    private String meetName;

    /** 人员ID */
    @Excel(name = "人员ID")
    @ApiModelProperty("人员ID")
    private Long userId;

    /** 姓名 */
    @Excel(name = "姓名")
    @ApiModelProperty("姓名")
    private String name;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty("联系电话")
    private String phone;

    /** 是否签到 */
    @Excel(name = "是否签到", dictType = "sys_yes_no")
    @ApiModelProperty("是否签到")
    private String isSign;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("meetId", getMeetId())
            .append("userId", getUserId())
            .append("name", getName())
            .append("phone", getPhone())
            .append("isSign", getIsSign())
            .toString();
    }
}
