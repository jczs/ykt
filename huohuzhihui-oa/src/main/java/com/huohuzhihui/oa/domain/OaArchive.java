package com.huohuzhihui.oa.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.system.domain.SysFiles;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 文档轮阅对象 oa_archive
 * 
 * @author yepanpan
 * @date 2020-12-15
 */
@ApiModel("文档轮阅实体")
@Getter
@Setter
public class OaArchive extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    private Long users[];
    private String userNames;

    /** 工作流 */
    @Excel(name = "工作流")
    @ApiModelProperty("工作流")
    private String title;

    /** 附件 */
    @ApiModelProperty("附件")
    private Long fileId;
    private SysFiles file;

    /** 来源 */
    @Excel(name = "来源")
    @ApiModelProperty("来源")
    private String source;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("发布时间")
    private Date addTime;

    /** 发布人 */
    @ApiModelProperty("发布人")
    private Long addUser;
    @Excel(name = "发布人")
    private String addUserName;

    /** 说明 */
    @Excel(name = "说明")
    @ApiModelProperty("说明")
    private String comment;

    /** 是否读完 */
    @Excel(name = "是否读完", dictType = "sys_yes_no")
    @ApiModelProperty("是否读完")
    private String status;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("fileId", getFileId())
            .append("source", getSource())
            .append("addTime", getAddTime())
            .append("addUser", getAddUser())
            .append("comment", getComment())
            .append("status", getStatus())
            .toString();
    }
}
