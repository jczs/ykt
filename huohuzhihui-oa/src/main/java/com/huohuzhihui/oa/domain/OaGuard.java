package com.huohuzhihui.oa.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 门禁对象 oa_guard
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("门禁实体")
@Getter
@Setter
public class OaGuard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 编码 */
    @Excel(name = "编码")
    @ApiModelProperty("编码")
    private String code;

    /** 门禁名称 */
    @Excel(name = "门禁名称")
    @ApiModelProperty("门禁名称")
    private String title;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty("状态")
    private String status;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("title", getTitle())
            .append("status", getStatus())
            .toString();
    }
}
