package com.huohuzhihui.oa.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 出入记录对象 oa_inout
 * 
 * @author yepanpan
 * @date 2020-12-10
 */
@ApiModel("出入记录实体")
@Getter
@Setter
public class OaInout extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 门禁ID */
    @ApiModelProperty("门禁ID")
    private Long guardId;
    @Excel(name = "门禁")
    private String guardName;

    /** 日期 */
    @Excel(name = "日期")
    @ApiModelProperty("日期")
    private String day;

    /** 用户 */
    @ApiModelProperty("用户")
    private Long userId;
    
    /** 姓名 */
    @Excel(name = "姓名")
    @ApiModelProperty("姓名")
    private String realName;

    /** 进入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "进入时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("进入时间")
    private Date inTime;

    /** 离开时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "离开时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("离开时间")
    private Date outTime;

    /** 备注 */
    private String comment;


    /** 微信ID */
    private String openId;


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("guardId", getGuardId())
            .append("userId", getUserId())
            .append("inTime", getInTime())
            .append("outTime", getOutTime())
            .append("comment", getComment())
            .append("realName", getRealName())
            .append("day", getDay())
            .append("openId", getOpenId())
            .toString();
    }
}
