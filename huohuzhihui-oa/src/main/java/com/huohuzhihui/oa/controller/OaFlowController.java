package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaFlow;
import com.huohuzhihui.oa.service.IOaFlowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工作流Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("工作流信息管理")
@RestController
@RequestMapping("/oa/flow")
public class OaFlowController extends BaseController
{
    @Autowired
    private IOaFlowService oaFlowService;

    /**
     * 查询工作流列表
     */
    @ApiOperation("获取工作流列表")
    @PreAuthorize("@ss.hasPermi('oa:flow:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaFlow oaFlow)
    {
        startPage();
        List<OaFlow> list = oaFlowService.selectOaFlowList(oaFlow);
        return getDataTable(list);
    }
    

    /**
     * 查询工作流列表
     */
    @ApiOperation("获取工作流列表")
    @GetMapping("/select")
    public TableDataInfo select(OaFlow oaFlow)
    {
        startPage();
        List<OaFlow> list = oaFlowService.selectOaFlowList(oaFlow);
        return getDataTable(list);
    }

    /**
     * 导出工作流列表
     */
    @ApiOperation("导出工作流列表")
    @PreAuthorize("@ss.hasPermi('oa:flow:export')")
    @Log(title = "工作流", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaFlow oaFlow)
    {
        List<OaFlow> list = oaFlowService.selectOaFlowList(oaFlow);
        ExcelUtil<OaFlow> util = new ExcelUtil<OaFlow>(OaFlow.class);
        return util.exportExcel(list, "flow");
    }

    /**
     * 获取工作流详细信息
     */
    @ApiOperation("获取工作流详细信息")
    @ApiImplicitParam(name = "id", value = "工作流ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:flow:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaFlowService.selectOaFlowById(id));
    }

    /**
     * 新增工作流
     */
    @ApiOperation("新增工作流")
    @PreAuthorize("@ss.hasPermi('oa:flow:add')")
    @Log(title = "工作流", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaFlow oaFlow)
    {
        return toAjax(oaFlowService.insertOaFlow(oaFlow));
    }

    /**
     * 修改工作流
     */
    @ApiOperation("修改工作流")
    @PreAuthorize("@ss.hasPermi('oa:flow:edit')")
    @Log(title = "工作流", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaFlow oaFlow)
    {
        return toAjax(oaFlowService.updateOaFlow(oaFlow));
    }

    /**
     * 删除工作流
     */
    @ApiOperation("删除工作流")
    @ApiImplicitParam(name = "id", value = "工作流ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:flow:remove')")
    @Log(title = "工作流", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaFlowService.deleteOaFlowByIds(ids));
    }
}
