package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaMeeting;
import com.huohuzhihui.oa.service.IOaMeetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会议申请Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("会议申请信息管理")
@RestController
@RequestMapping("/oa/meeting")
public class OaMeetingController extends BaseController
{
    @Autowired
    private IOaMeetingService oaMeetingService;

    /**
     * 查询会议申请列表
     */
    @ApiOperation("获取会议申请列表")
    @PreAuthorize("@ss.hasPermi('oa:meeting:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaMeeting oaMeeting)
    {
        startPage();
        List<OaMeeting> list = oaMeetingService.selectOaMeetingList(oaMeeting);
        return getDataTable(list);
    }
    
    /**
     * 查询个人的会议申请列表
     */
    @ApiOperation("获取个人的会议申请列表")
    @PreAuthorize("@ss.hasPermi('oa:meeting:my')")
    @GetMapping("/my")
    public TableDataInfo my(OaMeeting oaMeeting)
    {
        startPage();
        oaMeeting.setAddUser(SecurityUtils.getLoginUser().getUser().getUserId());
        List<OaMeeting> list = oaMeetingService.selectOaMeetingList(oaMeeting);
        return getDataTable(list);
    }

    /**
     * 导出会议申请列表
     */
    @ApiOperation("导出会议申请列表")
    @PreAuthorize("@ss.hasPermi('oa:meeting:export')")
    @Log(title = "会议申请", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaMeeting oaMeeting)
    {
        List<OaMeeting> list = oaMeetingService.selectOaMeetingList(oaMeeting);
        ExcelUtil<OaMeeting> util = new ExcelUtil<OaMeeting>(OaMeeting.class);
        return util.exportExcel(list, "会议信息");
    }

    /**
     * 获取会议申请详细信息
     */
    @ApiOperation("获取会议申请详细信息")
    @ApiImplicitParam(name = "id", value = "会议申请ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:meeting:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaMeetingService.selectOaMeetingById(id));
    }

    /**
     * 新增会议申请
     */
    @ApiOperation("新增会议申请")
    @PreAuthorize("@ss.hasPermi('oa:meeting:add')")
    @Log(title = "会议申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaMeeting oaMeeting)
    {
        return toAjax(oaMeetingService.insertOaMeeting(oaMeeting));
    }

    /**
     * 修改会议申请
     */
    @ApiOperation("修改会议申请")
    @PreAuthorize("@ss.hasPermi('oa:meeting:edit')")
    @Log(title = "会议申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaMeeting oaMeeting)
    {
        return toAjax(oaMeetingService.updateOaMeeting(oaMeeting));
    }

    /**
     * 删除会议申请
     */
    @ApiOperation("删除会议申请")
    @ApiImplicitParam(name = "id", value = "会议申请ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:meeting:remove')")
    @Log(title = "会议申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaMeetingService.deleteOaMeetingByIds(ids));
    }
    
    /**
     * 审核会议申请
     */
    @ApiOperation("审核会议申请")
    @PreAuthorize("@ss.hasPermi('oa:meeting:check')")
    @Log(title = "会议申请", businessType = BusinessType.UPDATE)
    @PostMapping("/check")
    public AjaxResult check(@RequestBody OaMeeting OaMeeting)
    {
        return toAjax(oaMeetingService.checkMeeting(OaMeeting));
    }
}
