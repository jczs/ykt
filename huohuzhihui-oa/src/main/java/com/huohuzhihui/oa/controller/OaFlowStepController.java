package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaFlowStep;
import com.huohuzhihui.oa.service.IOaFlowStepService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工作流步骤Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("工作流步骤信息管理")
@RestController
@RequestMapping("/oa/step")
public class OaFlowStepController extends BaseController
{
    @Autowired
    private IOaFlowStepService oaFlowStepService;

    /**
     * 查询工作流步骤列表
     */
    @ApiOperation("获取工作流步骤列表")
    @PreAuthorize("@ss.hasPermi('oa:step:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaFlowStep oaFlowStep)
    {
        startPage();
        List<OaFlowStep> list = oaFlowStepService.selectOaFlowStepList(oaFlowStep);
        return getDataTable(list);
    }

    /**
     * 导出工作流步骤列表
     */
    @ApiOperation("导出工作流步骤列表")
    @PreAuthorize("@ss.hasPermi('oa:step:export')")
    @Log(title = "工作流步骤", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaFlowStep oaFlowStep)
    {
        List<OaFlowStep> list = oaFlowStepService.selectOaFlowStepList(oaFlowStep);
        ExcelUtil<OaFlowStep> util = new ExcelUtil<OaFlowStep>(OaFlowStep.class);
        return util.exportExcel(list, "step");
    }

    /**
     * 获取工作流步骤详细信息
     */
    @ApiOperation("获取工作流步骤详细信息")
    @ApiImplicitParam(name = "id", value = "工作流步骤ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:step:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaFlowStepService.selectOaFlowStepById(id));
    }

    /**
     * 新增工作流步骤
     */
    @ApiOperation("新增工作流步骤")
    @PreAuthorize("@ss.hasPermi('oa:step:add')")
    @Log(title = "工作流步骤", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaFlowStep oaFlowStep)
    {
        return toAjax(oaFlowStepService.insertOaFlowStep(oaFlowStep));
    }

    /**
     * 修改工作流步骤
     */
    @ApiOperation("修改工作流步骤")
    @PreAuthorize("@ss.hasPermi('oa:step:edit')")
    @Log(title = "工作流步骤", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaFlowStep oaFlowStep)
    {
        return toAjax(oaFlowStepService.updateOaFlowStep(oaFlowStep));
    }

    /**
     * 删除工作流步骤
     */
    @ApiOperation("删除工作流步骤")
    @ApiImplicitParam(name = "id", value = "工作流步骤ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:step:remove')")
    @Log(title = "工作流步骤", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaFlowStepService.deleteOaFlowStepByIds(ids));
    }
    
    /**
     * 测试工作流步骤
     */
    @ApiOperation("获取工作流步骤详细信息")
    @ApiImplicitParam(name = "id", value = "工作流ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:step:add')")
    @GetMapping(value = "/test/{flowId}")
    public AjaxResult test(@PathVariable("flowId") Long flowId)
    {
        return AjaxResult.success(oaFlowStepService.testOaFlowStep(flowId));
    }
}
