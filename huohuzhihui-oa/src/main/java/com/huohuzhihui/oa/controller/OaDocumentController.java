package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaDocument;
import com.huohuzhihui.oa.service.IOaDocumentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 公文Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("公文信息管理")
@RestController
@RequestMapping("/oa/document")
public class OaDocumentController extends BaseController
{
    @Autowired
    private IOaDocumentService oaDocumentService;

    /**
     * 查询公文列表
     */
    @ApiOperation("获取公文列表")
    @PreAuthorize("@ss.hasPermi('oa:document:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaDocument oaDocument)
    {
        startPage();
        List<OaDocument> list = oaDocumentService.selectOaDocumentList(oaDocument);
        return getDataTable(list);
    }
    
    /**
     * 查询个人公文列表
     */
    @ApiOperation("获取个人公文列表")
    @PreAuthorize("@ss.hasPermi('oa:document:my')")
    @GetMapping("/my")
    public TableDataInfo my(OaDocument oaDocument)
    {
        startPage();
        oaDocument.setAddUser(SecurityUtils.getLoginUser().getUser().getUserId());
        List<OaDocument> list = oaDocumentService.selectOaDocumentList(oaDocument);
        return getDataTable(list);
    }
    

    /**
     * 导出公文列表
     */
    @ApiOperation("导出公文列表")
    @PreAuthorize("@ss.hasPermi('oa:document:export')")
    @Log(title = "公文", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaDocument oaDocument)
    {
        List<OaDocument> list = oaDocumentService.selectOaDocumentList(oaDocument);
        ExcelUtil<OaDocument> util = new ExcelUtil<OaDocument>(OaDocument.class);
        return util.exportExcel(list, "公文");
    }

    /**
     * 获取公文详细信息
     */
    @ApiOperation("获取公文详细信息")
    @ApiImplicitParam(name = "id", value = "公文ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:document:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaDocumentService.selectOaDocumentById(id));
    }

    /**
     * 新增公文
     */
    @ApiOperation("新增公文")
    @PreAuthorize("@ss.hasPermi('oa:document:add')")
    @Log(title = "公文", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaDocument oaDocument)
    {
        return toAjax(oaDocumentService.insertOaDocument(oaDocument));
    }

    /**
     * 修改公文
     */
    @ApiOperation("修改公文")
    @PreAuthorize("@ss.hasPermi('oa:document:edit')")
    @Log(title = "公文", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaDocument oaDocument)
    {
        return toAjax(oaDocumentService.updateOaDocument(oaDocument));
    }

    /**
     * 删除公文
     */
    @ApiOperation("删除公文")
    @ApiImplicitParam(name = "id", value = "公文ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:document:remove')")
    @Log(title = "公文", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaDocumentService.deleteOaDocumentByIds(ids));
    }
    
    /**
     * 撤销公文
     */
    @ApiOperation("撤销公文")
    @ApiImplicitParam(name = "id", value = "公文ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:document:remove')")
    @Log(title = "公文", businessType = BusinessType.DELETE)
	@GetMapping("/rollback/{ids}")
    public AjaxResult rollback(@PathVariable Long[] ids)
    {
        return toAjax(oaDocumentService.deleteOaDocumentByIds(ids));
    }
    
    /**
     * 发送公文详
     */
    @ApiOperation("发送公文详")
    @ApiImplicitParam(name = "id", value = "公文ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:document:add')")
    @GetMapping(value = "/send/{id}")
    public AjaxResult send(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaDocumentService.sendOaDocument(id));
    }
}
