package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.oa.domain.OaFlowStep;
import com.huohuzhihui.oa.mapper.OaFlowStepMapper;
import com.huohuzhihui.oa.service.IOaFlowStepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作流步骤Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaFlowStepServiceImpl implements IOaFlowStepService 
{
    @Autowired
    private OaFlowStepMapper oaFlowStepMapper;

    /**
     * 查询工作流步骤
     * 
     * @param id 工作流步骤ID
     * @return 工作流步骤
     */
    @Override
    public OaFlowStep selectOaFlowStepById(Long id)
    {
        return oaFlowStepMapper.selectOaFlowStepById(id);
    }

    /**
     * 查询工作流步骤列表
     * 
     * @param oaFlowStep 工作流步骤
     * @return 工作流步骤
     */
    @Override
    public List<OaFlowStep> selectOaFlowStepList(OaFlowStep oaFlowStep)
    {
        return oaFlowStepMapper.selectOaFlowStepList(oaFlowStep);
    }

    /**
     * 新增工作流步骤
     * 
     * @param oaFlowStep 工作流步骤
     * @return 结果
     */
    @Override
    public int insertOaFlowStep(OaFlowStep oaFlowStep)
    {
    	oaFlowStep.setCreateBy(SecurityUtils.getUsername());
        oaFlowStep.setCreateTime(DateUtils.getNowDate());
        return oaFlowStepMapper.insertOaFlowStep(oaFlowStep);
    }

    /**
     * 修改工作流步骤
     * 
     * @param oaFlowStep 工作流步骤
     * @return 结果
     */
    @Override
    public int updateOaFlowStep(OaFlowStep oaFlowStep)
    {
    	oaFlowStep.setUpdateBy(SecurityUtils.getUsername());
        oaFlowStep.setUpdateTime(DateUtils.getNowDate());
        return oaFlowStepMapper.updateOaFlowStep(oaFlowStep);
    }

    /**
     * 批量删除工作流步骤
     * 
     * @param ids 需要删除的工作流步骤ID
     * @return 结果
     */
    @Override
    public int deleteOaFlowStepByIds(Long[] ids)
    {
        return oaFlowStepMapper.deleteOaFlowStepByIds(ids);
    }

    /**
     * 删除工作流步骤信息
     * 
     * @param id 工作流步骤ID
     * @return 结果
     */
    @Override
    public int deleteOaFlowStepById(Long id)
    {
        return oaFlowStepMapper.deleteOaFlowStepById(id);
    }
    
    
    /**
     * 测试工作流步骤
     * 
     * @param flowId Long 工作流ID
     * @return 结果
     */
    @Override
    public int testOaFlowStep(Long flowId) {
    	OaFlowStep cond = new OaFlowStep();
    	cond.setFlowId(flowId);
    	List<OaFlowStep> list = oaFlowStepMapper.selectOaFlowStepList(cond);
    	OaFlowStep start = null;
    	for(OaFlowStep step:list) {
    		if("1".equals(step.getType())){
    			start = step;
    		}
    	}
    	if(start == null) {
			throw new CustomException("没有起始流程");
    	}
    	
    	while(!"9".equals(start.getType())) {
    		start = getNextStep(start, list);
    		if(start == null) {
    			throw new CustomException("流程没有终结");
    		}else if("9".equals(start.getType())) {
    			break;
    		}
    	}
    	
    	return list.size();
    }
    
    
    public OaFlowStep getNextStep(OaFlowStep start, List<OaFlowStep> list) {
    	for(OaFlowStep step:list) {
    		if(step.getPid().equals(start.getId())){
    			return step;
    		}
    	}
    	return null;
    }
}
