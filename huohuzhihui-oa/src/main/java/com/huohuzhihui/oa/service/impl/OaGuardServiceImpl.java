package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.oa.domain.OaGuard;
import com.huohuzhihui.oa.mapper.OaGuardMapper;
import com.huohuzhihui.oa.service.IOaGuardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 门禁Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaGuardServiceImpl implements IOaGuardService 
{
    @Autowired
    private OaGuardMapper oaGuardMapper;

    /**
     * 查询门禁
     * 
     * @param id 门禁ID
     * @return 门禁
     */
    @Override
    public OaGuard selectOaGuardById(Long id)
    {
        return oaGuardMapper.selectOaGuardById(id);
    }

    /**
     * 查询门禁列表
     * 
     * @param oaGuard 门禁
     * @return 门禁
     */
    @Override
    public List<OaGuard> selectOaGuardList(OaGuard oaGuard)
    {
        return oaGuardMapper.selectOaGuardList(oaGuard);
    }

    /**
     * 新增门禁
     * 
     * @param oaGuard 门禁
     * @return 结果
     */
    @Override
    public int insertOaGuard(OaGuard oaGuard)
    {
        return oaGuardMapper.insertOaGuard(oaGuard);
    }

    /**
     * 修改门禁
     * 
     * @param oaGuard 门禁
     * @return 结果
     */
    @Override
    public int updateOaGuard(OaGuard oaGuard)
    {
        return oaGuardMapper.updateOaGuard(oaGuard);
    }

    /**
     * 批量删除门禁
     * 
     * @param ids 需要删除的门禁ID
     * @return 结果
     */
    @Override
    public int deleteOaGuardByIds(Long[] ids)
    {
        return oaGuardMapper.deleteOaGuardByIds(ids);
    }

    /**
     * 删除门禁信息
     * 
     * @param id 门禁ID
     * @return 结果
     */
    @Override
    public int deleteOaGuardById(Long id)
    {
        return oaGuardMapper.deleteOaGuardById(id);
    }
}
