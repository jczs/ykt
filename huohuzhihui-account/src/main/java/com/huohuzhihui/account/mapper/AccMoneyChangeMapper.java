package com.huohuzhihui.account.mapper;

import java.util.List;
import com.huohuzhihui.account.domain.AccMoneyChange;

/**
 * 帐户增款Mapper接口
 * 
 * @author huohuzhihui
 * @date 2021-08-11
 */
public interface AccMoneyChangeMapper 
{
    /**
     * 查询帐户增款
     * 
     * @param id 帐户增款ID
     * @return 帐户增款
     */
    public AccMoneyChange selectAccMoneyChangeById(Long id);

    /**
     * 查询帐户增款列表
     * 
     * @param accMoneyChange 帐户增款
     * @return 帐户增款集合
     */
    public List<AccMoneyChange> selectAccMoneyChangeList(AccMoneyChange accMoneyChange);

    /**
     * 新增帐户增款
     * 
     * @param accMoneyChange 帐户增款
     * @return 结果
     */
    public int insertAccMoneyChange(AccMoneyChange accMoneyChange);

    /**
     * 修改帐户增款
     * 
     * @param accMoneyChange 帐户增款
     * @return 结果
     */
    public int updateAccMoneyChange(AccMoneyChange accMoneyChange);

    /**
     * 删除帐户增款
     * 
     * @param id 帐户增款ID
     * @return 结果
     */
    public int deleteAccMoneyChangeById(Long id);

    /**
     * 批量删除帐户增款
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAccMoneyChangeByIds(Long[] ids);
}
