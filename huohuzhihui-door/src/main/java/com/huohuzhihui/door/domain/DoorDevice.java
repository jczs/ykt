package com.huohuzhihui.door.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 门禁设备对象 t_door_device
 * 
 * @author huohuzhihui
 * @date 2021-08-09
 */
public class DoorDevice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** sn */
    @Excel(name = "sn")
    private String deviceSn;

    /** ip */
    @Excel(name = "ip")
    private String deviceIp;

    /** 端口号 */
    @Excel(name = "端口号")
    private Integer devicePort;

    /** 设备分配状态：0待分配1已分配 */
    @Excel(name = "设备分配状态：0待分配1已分配")
    private String useStatus;

    /** 删除标记 */
    private Integer delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDeviceSn(String deviceSn) 
    {
        this.deviceSn = deviceSn;
    }

    public String getDeviceSn() 
    {
        return deviceSn;
    }
    public void setDeviceIp(String deviceIp) 
    {
        this.deviceIp = deviceIp;
    }

    public String getDeviceIp() 
    {
        return deviceIp;
    }
    public void setDevicePort(Integer devicePort)
    {
        this.devicePort = devicePort;
    }

    public Integer getDevicePort()
    {
        return devicePort;
    }
    public void setUseStatus(String useStatus) 
    {
        this.useStatus = useStatus;
    }

    public String getUseStatus() 
    {
        return useStatus;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deviceSn", getDeviceSn())
            .append("deviceIp", getDeviceIp())
            .append("remark", getRemark())
            .append("devicePort", getDevicePort())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("useStatus", getUseStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
