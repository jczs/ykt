package com.huohuzhihui.door.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.door.domain.DoorRecord;
import com.huohuzhihui.door.service.IDoorRecordService;
import com.huohuzhihui.door.service.IDoorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 门禁记录Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-22
 */
@RestController
@RequestMapping("/door/record")
public class DoorRecordController extends BaseController
{
    @Autowired
    private IDoorRecordService doorRecordService;
    @Autowired
    private IDoorService doorService;

    /**
     * 查询门禁记录列表
     */
    @PreAuthorize("@ss.hasPermi('door:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(DoorRecord doorRecord)
    {
        startPage();
        List<DoorRecord> list = doorRecordService.selectDoorRecordList(doorRecord);
        return getDataTable(list);
    }

    /**
     * 导出门禁记录列表
     */
    @PreAuthorize("@ss.hasPermi('door:record:export')")
    @Log(title = "门禁记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DoorRecord doorRecord)
    {
        List<DoorRecord> list = doorRecordService.selectDoorRecordList(doorRecord);
        ExcelUtil<DoorRecord> util = new ExcelUtil<DoorRecord>(DoorRecord.class);
        return util.exportExcel(list, "record");
    }

    /**
     * 获取门禁记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('door:record:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(doorRecordService.selectDoorRecordById(id));
    }

    @GetMapping(value = "/device2local")
    public AjaxResult device2local(Long doorId)
    {
        try{
            doorRecordService.device2local(this.doorService.selectDoorById(doorId));
            return AjaxResult.success("同步门禁设备中进出记录成功");
        }catch (CustomException e){
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }


    /**
     * 新增门禁记录
     */
    @PreAuthorize("@ss.hasPermi('door:record:add')")
    @Log(title = "门禁记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DoorRecord doorRecord)
    {
        return toAjax(doorRecordService.insertDoorRecord(doorRecord));
    }

    /**
     * 修改门禁记录
     */
    @PreAuthorize("@ss.hasPermi('door:record:edit')")
    @Log(title = "门禁记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DoorRecord doorRecord)
    {
        return toAjax(doorRecordService.updateDoorRecord(doorRecord));
    }

    /**
     * 删除门禁记录
     */
    @PreAuthorize("@ss.hasPermi('door:record:remove')")
    @Log(title = "门禁记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(doorRecordService.deleteDoorRecordByIds(ids));
    }
}
