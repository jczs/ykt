package com.huohuzhihui.door.service;

import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.door.domain.Door;
import com.huohuzhihui.door.domain.DoorAuth;
import com.huohuzhihui.door.domain.DoorRecord;

import java.util.List;

public interface IDoorDeviceOperService {

    /**
     * 将权限写入控制器
     * @param sn 控制器sn
     * @param ip 控制器ip
     * @param port 控制器端口
     * @param lockNo 控制器锁号
     * @param cardNo 卡号
     * @param startTime 授权开始时间 格式：yyyyMMdd
     * @param endTime 授权截止时间 格式：yyyyMMdd
     * @return 授权结果：=1为授权成功1条
     */
    public int writeAuth(String sn,String ip,int port,int lockNo,long cardNo,String startTime,String endTime)throws CustomException;

    /**
     * 查询进出记录
     * @param sn 控制器sn
     * @param ip 控制器ip
     * @param port 控制器端口
     * @param lockNo 控制器锁号
     * @return
     */
    public List<DoorRecord> findInoutRecord(String sn,String ip,int port,int lockNo)throws CustomException;


    public String getReasonDetailChinese(int Reason);

}
